import tasklib
from tasklib import TaskWarrior

tw = tasklib.TaskWarrior(data_location="~/.task", create=False)
def fetchTasks(tw):
    """Gathers recurring tasks and places them in a list."""
    tasks = tw.tasks.filter('-CHILD', status = "recurring", tags__contains = ["HABIT"])
    print("hi")
    return tasks

def processData(mask):
    """This function gleans useful metrics from the mask property of each entry in the dictionary."""
    # - pending, + completed, X deleted, W waiting
    length = len(mask)
    pending = 0
    completed = 0
    deleted = 0
    waiting = 0
    for character in mask:
        if character == "-":
            pending += 1
        if character == "+":
            completed += 1

        if character == "X":
            deleted += 1

        if character == "W":
            waiting += 1
    # All stats gathered
    percent_pending = 0
    percent_completed = 0
    percent_del = 0
    percent_wait = 0

    if length > 0:
        percent_pending = pending/length
        percent_completed = completed/length
        percent_del = deleted/length
        percent_wait = waiting/length         
    if deleted > 0:
        completed_deleted = completed/deleted
    completed_deleted = 0
    pending_status = "one"
    if pending > 1:
        pending_status = "over"
    elif pending < 1:
        pending_status = "zero"
    taskTuple = (percent_pending, percent_completed, percent_del, percent_wait, completed_deleted, pending_status)
    return taskTuple
        
def outputData(taskTuple):
    """This function will use processData to get useful metrics and then output those metrics to the command-line."""
    if taskTuple[5] != "one":
        print("THIS TASK HAS MORE THAN ONE INSTANCE")
    print((taskTuple[0] * 100))
    print("% complete")

tasks = fetchTasks(tw)    
tuple = processData("xx--++++-")
outputData(tuple)
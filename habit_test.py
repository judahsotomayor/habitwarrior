import unittest
import tasklib

from unittest import (
        TestCase,
        )
from tasklib import (
        TaskWarrior,
)
from habit import (
        fetchTasks,
        processData,
        outputData,
)
# TODO @Nathan Write a function that takes a string for input.
# It must return true if the string only contains -, +, W, X. Otherwise,
# return false.
def is_mask(mask):
    acceptable_characters = "-+WX"
    for i in range(0, len(mask)):
        if mask[i] not in acceptable_characters:
            return False
    else:
        return True


class testDataProcessing(unittest.TestCase):
    def setUp(self):
        tw = tasklib.TaskWarrior(data_location='./taskTest', create=False) # TODO Set to actual directory of tasks
        self.tasks = tw.tasks.filter('-CHILD', status = "recurring", tags__contains = ["HABIT"])
        """
        Set up
        """
    def test_valid_masks_completed(self):
        """
        Make sure valid mask data gets a sensible response
        """    
        assert processData("+++++++++")[1] == 1
        assert processData("+++++-----")[1] == 0.5
        assert processData("++------")[1] == 0.25
        assert processData("")[1] == 0
        assert processData("--++WXWX") == 0.25
        assert processData("WXWXWXWXWX") == 0
         

if __name__ == '__main__':
    unittest.main()

---
id: bwkdt8868nsp6l02hng4daw
title: Habitwarrior Py
desc: ''
updated: 1671566603774
created: 1671564360821
---
Habitwarrior's design goals
* Simplify tracking habits in Taskwarrior. Currently it just uses recursive tasks, with no unified interface for habits.
* Make it easier to view data about habit completion.
* Provide useful statistics on completion/deletion ratio.
* Differentiate between habits and recurring tasks, while still using taskwarrior recurrence
# Antifeatures:
- Should not handle adding or completing tasks. Data only, at least in Python scripts

# Implementation Goals:
* Simple code
* Well-documented
* Tests included
* Single Python file
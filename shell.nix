{ pkgs ? import <nixpkgs> {}
}:

pkgs.mkShell {
  buildInputs = [
    pkgs.taskwarrior
    pkgs.python39
    pkgs.python39Packages.tasklib
    pkgs.python39Packages.pytest
    ];
}


